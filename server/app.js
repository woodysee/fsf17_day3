// Reference: http://expressjs.com/en/api.html
// e.g. Port number: 1024 is reserved

var express = require('express');//take in express middleware to create a web app server
var app = express();//creates an Express object with the name "app"

console.log(`You are now at: ${__dirname}`);//print out the directory where I am on
const NODE_PORT = process.env.PORT || 4000; //take a port from my env variables (e.g. export PORT=4000 for Mac; set PORT=4000 for Windows; run `env` in Terminal to check PORT env variable). It will take the environment before the fallback of 4000.
console.log(`The port on this machine using this code is: ${NODE_PORT}`); //prints "4000"

app.use(express.static(__dirname + '/../client/'));//tell Express where all my client files are to serve

//app is the middleware object. get is a method to obtain data from server to client.

//Testing database
let students = [
    {
        id: 1,
        name: "Kenneth",
        age: 25
    },
    {
        id: 2,
        name: "Alex",
        age: 26
    }
];

//Methods
app.get(
    "/students",
    (req,res) => {
        console.log(`200: Accessing /students`);
        res.status(200).json(students);
    }
);

app.get(
    "/students/:student_id",
    (req, res) => {
        console.log(req.params.student_id);
        let studentId = req.params.student_id;
        let student = null;
        for (var i = 0; i < students.length; i++) {
            if (students[i].id == studentId) {
                student = students[i];
                break;
            };
        };
        res.status(200).json(student);
    }
);


var middleware = 0;
//next is an initialiser endpoint
app.use(
    (req, res, next) => {
        console.log("Middleware 1");
        middleware += 1;
        next();
    }
);

app.use(
    (req, res, next) => {
        console.log("Middleware 2");
        middleware += 1;
        next();
    }
);

app.use(
    (req,res) => {
        var errTest = 4;
        try {
            console.log('Middleware Count:' + middleware);
            if (errTest != 5) {
                throw new Error("Not equals to 5");
            };
            console.log("404: Nothing was found!");
            res.send("<h1>404: Nothing was found!</h1>");
        } catch(exception) {
            console.log(`exception == ${exception}`);
            res.status(500).send("500: Something wrong with express and Middleware Count: " + middleware);
        }
        
    }
);

app.listen(
    NODE_PORT,
    () => {
        console.log(`My first node.js web app at ${NODE_PORT} with a middleware count of ${middleware}`);
        //console.log('My first node.js web app at' + NODE_PORT);
    }
);